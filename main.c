#include <stdio.h>
#include <stdlib.h>
// Proszę napisać kalkulator do wykonywania czterech podstawowych działań (+ - * / ). Wszystkie zmienne używane w obliczeniach powinny być alokowane dynamicznie w trakcie wykonywania programu.
// Program powinien mieć następujące opcje:
// Wprowadź dane (a i b) (w tym punkcie następuje alokacja pamięci)
// Wyświetl dane (a i b)
// Suma (a + b)
// Różnica (a - b)
// Iloczyn (a * b)
// Iloraz (a / b)
// Usuń dane (a i b) (w tym punkcie następuje zwolnienie pamięci)
// Dodatkowo:
// błędna kolejność operacji nie powoduje "problemów" (ale wyświetlamy komunikat)
// brak wycieków pamięci
// liczenia sumy i iloczynu dowolnej ilości liczb (na 6)


//  int *pointer;
//         pointer = malloc(1 * sizeof(int));
//         scanf(" %d",pointer);
//         printf("\n%d\n",*pointer);


void  menu(int *pSelect){
  
        printf("\n");
        printf("Wybierz opcje \n");
        printf(" \n");

        printf("1. Wprowadz Dane  \n");
        printf("2. Wyswietl Dane \n");
        printf("3. Suma \n");
        printf("4. Roznica \n");
        printf("5. Iloczyn \n");
        printf("6. Iloraz \n");
        printf("7. Usun Dane \n");

   
        printf("0. Koniec Programu \n");
        printf(" \n");

        scanf(" %d",pSelect);
}

void insert_Data(int *pNumber_Of_Data, float *pArray ){

        int *pCounter = malloc(1 * sizeof(int));
        *pCounter = 0;
        
        for(;*pCounter<*pNumber_Of_Data;){
                scanf("%f",&pArray[*pCounter]);
                *pCounter=*pCounter+1;
                
        }
        free(pCounter);

}

void print_Data(int *pNumber_Of_Data, float *pArray){

        int *pCounter = malloc(1 * sizeof(int));
        *pCounter = 0;
        for(;*pCounter<*pNumber_Of_Data;){
         
        printf("%d =  %f \n",*pCounter,pArray[*pCounter]);
                *pCounter=*pCounter+1;
        }
        free(pCounter);
}

void sum(int *pNumber_Data_To_Sum, float *pArray){

        float *pSum = malloc(1* sizeof(float));
        int *ptemp = malloc(1*sizeof(int));
        int *pCounter = malloc(1*sizeof(int));
        *pSum =0;
        *pCounter =0;
        for(;*pCounter<*pNumber_Data_To_Sum;){
                printf("Podaj indeks \n");
                scanf(" %d",ptemp);
                *pSum = *pSum + pArray[*ptemp];
                *pCounter=*pCounter+1;
                
        }

        printf("Suma = %f\n",*pSum);
        free(ptemp);
        free(pSum);
        free(pCounter);
        
}

void subtraction(int *pNumber_Data_To_Sub, float *pArray){

        float *pSubtraction = malloc(1* sizeof(float));
        int *ptemp = malloc(1*sizeof(int));
        int *pCounter = malloc(1*sizeof(int));
        printf("Podaj indeks \n");
        scanf(" %d",ptemp);
        *pSubtraction = pArray[*ptemp];
        *pCounter =1;
        for(;*pCounter<*pNumber_Data_To_Sub;){
                printf("Podaj indeks \n");
                scanf(" %d",ptemp);
                *pSubtraction = *pSubtraction - pArray[*ptemp];
                *pCounter=*pCounter+1;
                
        }

        printf("Wynik = %f\n",*pSubtraction);
        free(ptemp);
        free(pSubtraction);
        free(pCounter);
        
}

void divi(float *pArray){

        float *pResults = malloc(1 * sizeof(float));
        int *pTemp1 = malloc(1* sizeof(int));
        int *pTemp2 = malloc(1* sizeof(int));
        float *pResults1 = malloc(1 * sizeof(float));
        float *pResults2 = malloc(1 * sizeof(float));
        printf("Podaj indeks 1\n");
        scanf(" %d",pTemp1);
        printf("Podaj indeks 2\n");
        scanf(" %d",pTemp2);
        *pResults1 = pArray[*pTemp1];
        *pResults2 = pArray[*pTemp2];
        
       
        *pResults = *pResults1 / *pResults2;
        printf("Wynik = %f \n", *pResults);
        free(pResults);
        free(pTemp1);
        free(pTemp2);


}

void multiplication(int *pNumber_Data_To_Multi, float *pArray ){

        float *pResult = malloc(1* sizeof(float));
        int *ptemp = malloc(1*sizeof(int));
        int *pCounter = malloc(1*sizeof(int));
        *pResult = 1;
        *pCounter =0;
        for(;*pCounter<*pNumber_Data_To_Multi;){
                printf("Podaj indeks \n");
                scanf(" %d",ptemp);
                *pResult = *pResult * pArray[*ptemp];
                *pCounter=*pCounter+1;
                
        }

        printf("Suma = %f\n",*pResult);
        free(ptemp);
        free(pResult);
        free(pCounter);
}



int main(){
        
        int *pSelect = malloc(1 * sizeof(int));
        int *pNumber_Of_Data = malloc(1 * sizeof(int));
        float *pArray = malloc(*pNumber_Of_Data * sizeof(float));
        int *pNumber_Data_To_Use = malloc(1 * sizeof(int));
        pArray = NULL;

       
        do{
                menu(pSelect);
                switch(*pSelect)
                {
                case 1:
                        if(pArray != NULL){
                                free(pArray);
                        }
                        pArray = NULL;
                        
                        printf("Podaj ile danych chcesz wprowadzic \n");
                        scanf(" %d",pNumber_Of_Data);
                        pArray = malloc(*pNumber_Of_Data * sizeof(float));
                        insert_Data(pNumber_Of_Data, pArray);
                        break;
                case 2:
                        if(pArray == NULL){
                                printf("Brak liczb w tablicy \n");
                                break;
                        }else{
                                print_Data(pNumber_Of_Data, pArray);
                        }

                        break;
                case 3:
                        if(pArray == NULL){
                                printf("Brak liczb w tablicy \n");
                                break;
                        }else{
                                printf("Podaj ile chcesz liczb sumowac \n");
                                scanf(" %d",pNumber_Data_To_Use);
                                sum(pNumber_Data_To_Use,pArray);
                        }
                        break;
                case 4:
                        if(pArray == NULL){
                                printf("Brak liczb w tablicy \n");
                                break;
                        }else{
                                printf("Podaj ile chcesz liczb odjac \n");
                                scanf(" %d",pNumber_Data_To_Use);
                                subtraction(pNumber_Data_To_Use,pArray);
                        }
                        break;
                case 5:
                        if(pArray == NULL){
                                printf("Brak liczb w tablicy \n");
                                break;
                        }else{
                                printf("Podaj ile chcesz liczb mnozyc \n");
                                scanf(" %d",pNumber_Data_To_Use);
                                multiplication(pNumber_Data_To_Use,pArray);
                        }
               
                        break;
                case 6:
                        if(pArray == NULL){
                                printf("Brak liczb w tablicy \n");
                                break;
                        }else{
                                
                                divi(pArray);
                        }

                        break;
                case 7:
                        free(pArray);
                        pArray = NULL;
                        break;
                }
        }while(*pSelect!=0); 

        free(pSelect);
        free(pNumber_Of_Data);
        free(pNumber_Data_To_Use);
        
       
       



        return 0;
}