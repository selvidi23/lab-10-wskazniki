# README #

// Proszę napisać kalkulator do wykonywania czterech podstawowych działań (+ - * / ). Wszystkie zmienne używane w obliczeniach powinny być alokowane dynamicznie w trakcie wykonywania programu.
// Program powinien mieć następujące opcje:
// Wprowadź dane (a i b) (w tym punkcie następuje alokacja pamięci)
// Wyświetl dane (a i b)
// Suma (a + b)
// Różnica (a - b)
// Iloczyn (a * b)
// Iloraz (a / b)
// Usuń dane (a i b) (w tym punkcie następuje zwolnienie pamięci)
// Dodatkowo:
// błędna kolejność operacji nie powoduje "problemów" (ale wyświetlamy komunikat)
// brak wycieków pamięci
// liczenia sumy i iloczynu dowolnej ilości liczb (na 6)
